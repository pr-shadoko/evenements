package com.appspanel.evenements;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 *
 */
public class ActusAdapter extends RecyclerView.Adapter<ActusAdapter.EventViewHolder> {
    private RealmResults<Event> mEvents;
    private Context mContext;//Only needed to access string resources
    private OnEventClickListener mOnEventClickListener;

    public ActusAdapter(Context context) {
        Realm realm = Realm.getDefaultInstance();
        mEvents = realm.where(Event.class).findAll();
        mContext = context.getApplicationContext(); //So there is no need to keep track of the activity we are attached to
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        mOnEventClickListener = onEventClickListener;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.actus_item, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        Event event = mEvents.get(position);
        Drawable picture = Drawable.createFromStream(new ByteArrayInputStream(event.getPictureData()), null);
        holder.mPicture.setImageDrawable(picture);
        holder.mType.setText(event.getType());
        holder.mTime.setText(event.getTime());
        double distance = event.getDistance();
        holder.mDistance.setText(
              distance <= 1000 ?
                    String.format(mContext.getString(R.string.distance_m), Math.round(distance)) :
                    String.format(mContext.getString(R.string.distance_km), Math.round(distance / 1000))
        );
        double fee = event.getFee();
        holder.mFee.setText(
              fee == 0 ?
                    mContext.getString(R.string.no_fee) :
                    String.format(mContext.getString(R.string.fee), fee)
        );
        holder.mTitle.setText(event.getTitle());
        holder.mPlace.setText(event.getPlace());
        holder.mDate.setText(event.getDate());
        holder.mCity.setText(event.getCity());
        final long id = event.getId();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnEventClickListener != null) {
                    mOnEventClickListener.onEventClicked(id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    public interface OnEventClickListener {
        void onEventClicked(long id);
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        public final ImageView mPicture;
        public final TextView mType;
        public final TextView mTime;
        public final TextView mDistance;
        public final TextView mFee;
        public final TextView mTitle;
        public final TextView mPlace;
        public final TextView mDate;
        public final TextView mCity;

        public EventViewHolder(View itemView) {
            super(itemView);
            mPicture = (ImageView) itemView.findViewById(R.id.picture);
            mType = (TextView) itemView.findViewById(R.id.type);
            mTime = (TextView) itemView.findViewById(R.id.time);
            mDistance = (TextView) itemView.findViewById(R.id.distance);
            mFee = (TextView) itemView.findViewById(R.id.fee);
            mTitle = (TextView) itemView.findViewById(R.id.title);
            mPlace = (TextView) itemView.findViewById(R.id.place);
            mDate = (TextView) itemView.findViewById(R.id.date);
            mCity = (TextView) itemView.findViewById(R.id.city);
        }
    }
}
