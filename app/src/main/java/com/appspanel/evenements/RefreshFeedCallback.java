package com.appspanel.evenements;

/**
 *
 */
public interface RefreshFeedCallback {
    void onFeedRefreshed();
}
