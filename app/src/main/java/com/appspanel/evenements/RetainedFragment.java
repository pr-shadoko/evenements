package com.appspanel.evenements;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 *
 */
public class RetainedFragment extends Fragment implements FeedFetcher.FeedFetcherListener {
    private Thread mFeedFetcherThread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void startFeedFetcherThread(String url) {
        if(mFeedFetcherThread == null || !mFeedFetcherThread.isAlive()) {
            mFeedFetcherThread = new Thread(new FeedFetcher(url, this));
            mFeedFetcherThread.start();
        }
    }

    @Override
    public void onFeedFetched() {
        final Activity activity = getActivity();
        if(activity instanceof MainActivity) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((MainActivity) activity).notifyFeedRefreshed();
                }
            });
        }
    }

    @Override
    public void onConnectionError() {
        final Activity activity = getActivity();
        if(activity instanceof MainActivity) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((MainActivity) activity).displayMessage(R.string.error_connection);

                }
            });
        }
    }
}
