package com.appspanel.evenements;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ActusAdapter.OnEventClickListener {
    private static final String TAG_RETAINED_FRAGMENT = "com.appspanel.evenements.RETAINED_FRAGMENT";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private List<RefreshFeedCallback> mFeedRefreshObservers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //Set up the retained fragment to keep background tasks running
        getSupportFragmentManager()
              .beginTransaction()
              .add(new RetainedFragment(), TAG_RETAINED_FRAGMENT)
              .commit();
    }

    public void refreshFeed(String url) {
        RetainedFragment retainedFragment = getRetainedFragment();
        if(retainedFragment != null) {
            retainedFragment.startFeedFetcherThread(url);
        }
    }

    public void registerFeedRefreshObserver(RefreshFeedCallback callback) {
        mFeedRefreshObservers.add(callback);
    }

    public void unregisterFeedRefreshObserver(RefreshFeedCallback callback) {
        mFeedRefreshObservers.remove(callback);
    }

    public void notifyFeedRefreshed() {
        for(RefreshFeedCallback callback : mFeedRefreshObservers) {
            callback.onFeedRefreshed();
        }
    }

    public void displayMessage(int messageId) {
        displayMessage(getString(messageId));
    }

    public void displayMessage(String message) {
        //android.R.id.content represents the root view of the activity. It will *always* be there.
        //noinspection ConstantConditions
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }

    private RetainedFragment getRetainedFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG_RETAINED_FRAGMENT);
        if(fragment instanceof RetainedFragment) {
            return (RetainedFragment) fragment;
        }
        return null;
    }

    @Override
    public void onEventClicked(long id) {
        displayMessage("Coming: Display of full event");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //TODO: handle map button from ActusFragment as it should not be visible from other pages
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_map) {
            displayMessage("Coming: switch to map display mode");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private static final int FRAGMENTS_COUNT = 3;
        private static final int POSITION_ACTUS = 0;
        private static final int POSITION_INFOS = 1;
        private static final int POSITION_CONTACT = 2;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case POSITION_ACTUS:
                    return new ActusFragment();
                case POSITION_INFOS:
                    return new InfosFragment();
                case POSITION_CONTACT:
                    return new ContactFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return FRAGMENTS_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String[] titles = getResources().getStringArray(R.array.tabs_label);
            return titles[position];
        }
    }
}
