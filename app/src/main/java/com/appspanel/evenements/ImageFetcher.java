package com.appspanel.evenements;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 *
 */
public class ImageFetcher extends FutureTask<byte[]> {
    public ImageFetcher(String imageUrl) {
        super(new Fetcher(imageUrl));
    }

    private static class Fetcher implements Callable<byte[]> {
        private String url;

        public Fetcher(String url) {
            this.url = url;
        }

        @Override
        public byte[] call() throws Exception {
            try {
                URL imageUrl = new URL(url);
                InputStream inputStream = new BufferedInputStream(imageUrl.openStream());
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                int n = 0;
                while(-1 != (n = inputStream.read(buf))) {
                    outputStream.write(buf, 0, n);
                }
                outputStream.close();
                inputStream.close();
                return outputStream.toByteArray();
            } catch(IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
