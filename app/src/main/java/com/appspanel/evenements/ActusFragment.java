package com.appspanel.evenements;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 *
 */
public class ActusFragment extends Fragment implements RefreshFeedCallback {
    private static final String FEED_URL = "http://lagenda.apnl.ws/index.php?key=N25QS3JnOWg=&deviceuid=MTIzNDU2&action=Z2V0ZWx0cw==&quand=YWpk&lat=NDUuNzQ5MjIy&lng=NC44NTI3MDI=&dist=NTAuMjU3NzMy&gratuit=MQ==&start=MA==&end=NDA=&format=anNvbg==";

    private RecyclerView mEventList;
    private ActusAdapter mActusAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) context;
            mainActivity.registerFeedRefreshObserver(this);
            mActusAdapter = new ActusAdapter(mainActivity);
            mActusAdapter.setOnEventClickListener(mainActivity);
        }
    }

    @Override
    public void onDetach() {
        Activity activity = getActivity();
        if(activity instanceof MainActivity) {
            ((MainActivity) activity).unregisterFeedRefreshObserver(this);
        }
        mActusAdapter.setOnEventClickListener(null);
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_actus, container, false);
        mEventList = (RecyclerView) rootView.findViewById(R.id.event_list);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Activity activity = getActivity();
        if(activity instanceof MainActivity) {
            ((MainActivity) activity).refreshFeed(FEED_URL);
        }
        mEventList.setAdapter(mActusAdapter);
    }

    @Override
    public void onFeedRefreshed() {
        mActusAdapter.notifyDataSetChanged();
    }
}
