package com.appspanel.evenements;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 *
 */
public class Event extends RealmObject {
    @PrimaryKey
    private int mId;
    private int mAppId;
    private boolean mIsActive;
    private String mType;
    private double mLatitude;
    private double mLongitude;
    private String mTitle;
    private String mDescription;
    private String mPicturePath;
    private String mPlace;//data2
    private String mPhone;//data3
    private String mWebsite;//data5
    private String mCity;//data7
    private String mDate;//data8
    private String mTime;//data10
    private double mFee;//data11
    private double mDistance;//data12
    private String mDescriptionOut;
    private byte[] mPictureData;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getAppId() {
        return mAppId;
    }

    public void setAppId(int appId) {
        mAppId = appId;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public void setIsActive(boolean isActive) {
        mIsActive = isActive;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPicturePath() {
        return mPicturePath;
    }

    public void setPicturePath(String picturePath) {
        mPicturePath = picturePath;
    }

    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String place) {
        mPlace = place;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public void setWebsite(String website) {
        mWebsite = website;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String time) {
        mTime = time;
    }

    public double getFee() {
        return mFee;
    }

    public void setFee(double fee) {
        mFee = fee;
    }

    public double getDistance() {
        return mDistance;
    }

    public void setDistance(double distance) {
        mDistance = distance;
    }

    public String getDescriptionOut() {
        return mDescriptionOut;
    }

    public void setDescriptionOut(String descriptionOut) {
        mDescriptionOut = descriptionOut;
    }

    public byte[] getPictureData() {
        return mPictureData;
    }

    public void setPictureData(byte[] pictureData) {
        mPictureData = pictureData;
    }
}
