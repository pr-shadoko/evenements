package com.appspanel.evenements;

import android.util.Base64;
import android.util.JsonReader;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 *
 */
public class FeedFetcher implements Runnable {
    private static final String KEY_ID = "id_element";
    private static final String KEY_APP_ID = "id_appli";
    private static final String KEY_IS_ACTIVE = "active";
    private static final String KEY_TYPE = "type";
    private static final String KEY_LATITUDE = "lat";
    private static final String KEY_LONGITUDE = "lng";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_PICTURE = "picture";
    private static final String KEY_PLACE = "data2";
    private static final String KEY_PHONE = "data3";
    private static final String KEY_WEBSITE = "data5";
    private static final String KEY_CITY = "data7";
    private static final String KEY_DATE = "data8";
    private static final String KEY_TIME = "data10";
    private static final String KEY_FEE = "data11";
    private static final String KEY_DISTANCE = "data12";
    private static final String KEY_DESCRIPTION_OUT = "description_out";
    private static final String ENCODING_UTF8 = "UTF-8";
    private static final String ENCODING_ISO_8859_1 = "ISO-8859-1";

    private String mUrl;
    private FeedFetcherListener mListener;

    public FeedFetcher(String url, FeedFetcherListener listener) {
        mUrl = url;
        mListener = listener;
    }

    @Override
    public void run() {
        try {
            JsonReader reader = new JsonReader(new InputStreamReader(openConnection(mUrl)));
            List<Event> events = readFeed(reader);
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(events);
            realm.commitTransaction();
        } catch(IOException e) {
            e.printStackTrace();
            mListener.onConnectionError();
        }
        mListener.onFeedFetched();
    }

    private InputStream openConnection(String url) throws IOException {
        URL feedUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) feedUrl.openConnection();
        connection.setReadTimeout(10 * 1000);
        connection.setConnectTimeout(10 * 1000);
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        connection.connect();
        int response = connection.getResponseCode();
        if(response != 200) {
            throw new IOException("Invalid server response.");
        }
        return connection.getInputStream();
    }

    private List<Event> readFeed(JsonReader reader) throws IOException {
        List<Event> events = new ArrayList<>();
        reader.beginArray();

        while(reader.hasNext()) {
            events.add(readEvent(reader));
        }
        reader.endArray();
        return events;
    }

    private Event readEvent(JsonReader reader) throws IOException {
        Event event = new Event();
        reader.beginObject();
        while(reader.hasNext()) {
            String key = reader.nextName();
            byte[] base64Value = Base64.decode(reader.nextString(), Base64.DEFAULT);
            if(KEY_ID.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setId(Integer.valueOf(stringValue));
            } else if(KEY_APP_ID.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setAppId(Integer.valueOf(stringValue));
            } else if(KEY_IS_ACTIVE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setIsActive(Integer.valueOf(stringValue) == 1);
            } else if(KEY_TYPE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setType(stringValue);
            } else if(KEY_LATITUDE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setLatitude(Double.valueOf(stringValue));
            } else if(KEY_LONGITUDE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setLongitude(Double.valueOf(stringValue));
            } else if(KEY_TITLE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_ISO_8859_1);
                event.setTitle(stringValue);
            } else if(KEY_DESCRIPTION.equals(key)) {//Does not convert in my tests...
                String stringValue = new String(base64Value, ENCODING_ISO_8859_1);
                event.setDescription(stringValue);
            } else if(KEY_PICTURE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setPicturePath(stringValue);
                event.setPictureData(fetchPicture(stringValue));
            } else if(KEY_PLACE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_ISO_8859_1);
                event.setPlace(stringValue);
            } else if(KEY_PHONE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setPhone(stringValue);
            } else if(KEY_WEBSITE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setWebsite(stringValue);
            } else if(KEY_CITY.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setCity(stringValue);
            } else if(KEY_DATE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setDate(stringValue);
            } else if(KEY_TIME.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setTime(stringValue);
            } else if(KEY_FEE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setFee(Double.valueOf(stringValue));
            } else if(KEY_DISTANCE.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setDistance(Double.valueOf(stringValue));
            } else if(KEY_DESCRIPTION_OUT.equals(key)) {
                String stringValue = new String(base64Value, ENCODING_UTF8);
                event.setDescriptionOut(stringValue);
            } else {
                //Unused value. skip it.
            }
        }
        reader.endObject();
        return event;
    }

    private byte[] fetchPicture(String pictureUrl) {
        try {
            URL imageUrl = new URL(pictureUrl);
            InputStream inputStream = new BufferedInputStream(imageUrl.openStream());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n;
            while(-1 != (n = inputStream.read(buf))) {
                outputStream.write(buf, 0, n);
            }
            outputStream.close();
            inputStream.close();
            return outputStream.toByteArray();
        } catch(IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public interface FeedFetcherListener {
        void onFeedFetched();

        void onConnectionError();
    }
}